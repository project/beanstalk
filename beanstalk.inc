<?php

/**
 * @file
 * Provides admin page callbacks and helper functions for beanstalk module.
 */

/**
 * Configures the various Beanstalk options.
 */
function beanstalk_settings() {
  if (variable_get('beanstalk_taxonomy', 0) == 1 && count(_beanstalk_get_vocabularies()) == 1) {
    drupal_set_message(t('Please configure taxonomy to be able to categorize Beanstalk commits.'), 'error');
  }
  if (variable_get('beanstalk_og', 0) == 1 && !og_is_group_post_type('beanstalk_commit')) {
    drupal_set_message(t('Please configure og to accept Beanstalk commits as group posts.'), 'error');
  }
  
  $form = array();

  $form['beanstalk_general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['beanstalk_general']['beanstalk_author'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link commits to users'),
    '#options' => t('Enable automatic linking'),
    '#default_value' => variable_get('beanstalk_author', 1),
    '#description' => t('Try to associate Beanstalk commits to website users.'),
  );

  $form['beanstalk_general']['beanstalk_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Link commits to nodes'),
    '#options' => t('Enable automatic linking'),
    '#default_value' => variable_get('beanstalk_links', 1),
    '#description' => t('Try to associate tags in Beanstalk commit messages to website nodes. Links will be built from tags in the form of #NID, e.g. #123 will be expanded to node/123.'),
  );

  $form['beanstalk_general']['beanstalk_publish'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically publish'),
    '#options' => t('Automatically publish commits'),
    '#default_value' => variable_get('beanstalk_publish', 1),
    '#description' => t('Publish commit Beanstalk nodes immediately, i.e. without review.'),
  );

  $form['beanstalk_taxonomy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Taxonomy support'),
    '#collapsible' => TRUE,
    '#collapsed' => !module_exists('taxonomy'),
  );

  $form['beanstalk_taxonomy']['beanstalk_taxonomy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable taxonomy integration'),
    '#options' => t('Enable automatic tagging of commits.'),
    '#default_value' => variable_get('beanstalk_taxonomy', 0),
    '#disabled' => !module_exists('taxonomy'),
    '#description' => t('Beanstalk commit messages will automatically be tagged with taxonomy terms.'),
  );
  
  $form['beanstalk_taxonomy']['beanstalk_vid'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#options' => _beanstalk_get_vocabularies(),
    '#default_value' => variable_get('beanstalk_vid', 0),
    '#disabled' => (variable_get('beanstalk_taxonomy', 0) == 1) ? FALSE : TRUE,
    '#description' => t('Select the vocabulary that will be used to tag Beanstalk commits. Beanstalk commit messages have to be <a href="!url">configured</a> to be categorized using at least one vocabulary.', array('!url' => url('admin/content/taxonomy'))) . ((variable_get('beanstalk_taxonomy', 0) == 0) ? ' <br /><b>'. t('This option will only be available if taxonomy support is enabled.') .'</b>' : ''),
  );

  $form['beanstalk_og'] = array(
    '#type' => 'fieldset',
    '#title' => t('OG support'),
    '#collapsible' => TRUE,
    '#collapsed' => !module_exists('og'),
  );

  $form['beanstalk_og']['beanstalk_og'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable OG integration'),
    '#options' => t('Enable automatic assocition of commits to organic groups.'),
    '#default_value' => variable_get('beanstalk_og', 0),
    '#disabled' => !module_exists('og'),
    '#description' => t('Beanstalk commit messages can be configured to automatically be posted to a specific group. Beanstalk commit messages have to be <a href="!url">configured</a> as group posts: ', array('!url' => url('admin/og/og'))) . ((variable_get('beanstalk_og', 0) == 0) ? ' <br /><b>'. t('This option will only be available if organic groups support is enabled.') .'</b>' : ''),
  );

  $form['beanstalk_autosync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Auto Sync'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['beanstalk_autosync']['beanstalk_autosync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Auto Sync'),
    '#options' => t('Enable automatic assocition of commits to organic groups.'),
    '#default_value' => variable_get('beanstalk_autosync', 0),
    '#description' => t('Enable this to keep your repositories in sync with Beanstalk by its API. Note that you need to provide the Beanstalk credentials for an Beanstalk user with admin priveleges.'),
  );

  $form['beanstalk_autosync']['beanstalk_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Beanstalk account'),
    '#default_value' => variable_get('beanstalk_account', ''),
    '#description' => t('The account name you use to access your Beanstalk app. Like <strong>example</strong> for http://<em>example</em>.beanstalkapp.com.'),
  );

  $form['beanstalk_autosync']['beanstalk_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Beanstalk username'),
    '#default_value' => variable_get('beanstalk_username', ''),
    '#description' => t('Your Beanstalk user name.'),
  );

  $form['beanstalk_autosync']['beanstalk_password'] = array(
    '#type' => 'password',
    '#title' => t('Beanstalk password'),
    '#default_value' => variable_get('beanstalk_password', ''),
    '#description' => t('Your Beanstalk user password.'),
  );

  $form['beanstalk_autosync']['beanstalk_webhook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Beanstalk web hook url'),
    '#default_value' => variable_get('beanstalk_webhook_url', url('beanstalk/webhook', array('absolute' => TRUE))),
    '#description' => t("The full url to set the Beanstalk webhooks. You probably don't want to change this."),
  );

  $form['#submit'][] = 'beanstalk_settings_validate';
  $form['#submit'][] = 'beanstalk_settings_submit';
  return system_settings_form($form);
}

/**
 * Validate Beanstalk settings form.
 */
function beanstalk_settings_validate($form, &$form_state) {
  // Avoid reset password if it's empty.
  if (empty($form_state['values']['beanstalk_password'])) {
    unset($form_state['values']['beanstalk_password']);
  }
}

/**
 * Add/edit beanstalk repository
 */
function beanstalk_repository_edit($form_state, $rid = NULL) {
  if (!empty($rid)) {
    $repository = beanstalk_repository_load($rid);
  }
  
  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository name'),
    '#required' => TRUE,
    '#default_value' => isset($repository) ? $repository->title : NULL,
    '#description' => t('The name of this repository.'),
  );
  $form['general']['rid'] = array(
    '#type' => 'hidden',
    '#value' => isset($repository) ? $repository->rid : NULL,
  );
  $form['general']['token'] = array(
    '#type' => 'hidden',
    '#value' => isset($repository) ? $repository->token : NULL,
  );
  
  $form['taxonomy'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tags'),
    '#collapsible' => TRUE,
    '#collapsed' => (variable_get('beanstalk_taxonomy', 0) == 1) ? FALSE : TRUE,
  );
  $form['taxonomy']['tids'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Tags'),
    '#options' => _beanstalk_get_terms(),
    '#default_value' => ($repository && $repository->tids) ? $repository->tids : 0,
    '#disabled' => (variable_get('beanstalk_taxonomy', 0) == 1) ? FALSE : TRUE,
    '#description' => t('Select the tag for Beanstalk commits.') . ((variable_get('beanstalk_taxonomy', 0) == 0) ? '<br /><b>'. t('This option will only be available if taxonomy support is enabled') .'</b>' : ''),
  );
  
  $form['group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Group'),
    '#collapsible' => TRUE,
    '#collapsed' => (variable_get('beanstalk_og', 0) == 1) ? FALSE : TRUE,
  );
  $form['group']['gids'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Groups'),
    '#options' => _beanstalk_get_groups(),
    '#default_value' => ($repository && $repository->gids) ? $repository->gids : 0,
    '#disabled' => (variable_get('beanstalk_og', 0) == 1) ? FALSE : TRUE,
    '#description' => t('Select group to post Beanstalk commits to.') . ((variable_get('beanstalk_og', 0) == 0) ? '<br /><b>'. t('This option will only be available if OG support is enabled.') .'</b>' : ''),
  );
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Submit'),
  ); 

  return $form;
}


function beanstalk_repository_edit_submit($form, &$form_state) {
  
  $repository = array(
    'title' => $form_state['values']['title'],
    'token' => $form_state['values']['token'],
    'tids' => $form_state['values']['tids'],
    'gids' => $form_state['values']['gids'],
  );

  if (!empty($form_state['values']['rid'])) {
    $repository['rid'] = $form_state['values']['rid'];
    drupal_set_message(t('The repository %title has been updated.', array('%title' =>  $form_state['values']['title'])));
  }
  else {
    $repository['token'] = drupal_get_token($form_state['values']['title']);
    drupal_set_message(t('The repository %title has been created.', array('%title' =>  $form_state['values']['title'])));
  }
  
  beanstalk_repository_save($repository);
  $form_state['redirect'] = 'admin/settings/beanstalk/repositories';
}


/**
 * Menu callback
 * Displays an overview of all enabled repositories and associated webhook urls
 */
function beanstalk_repository_overview() {
  $headers = array(
    t('Name'),
    t('Beanstalk webhook target url'),
    array(
      'data' => t('Operations'), 
      'colspan' => 2
    ),
  );

  $result = db_query("SELECT * FROM {beanstalk_repository} ORDER BY title");
  $rows = array();   
  while ($repository = db_fetch_object($result)) {
    $rows[] = array(
      $repository->title,
      _beanstalk_webhook_url($repository->token),
      l(t('Edit'), 'admin/settings/beanstalk/repositories/edit/'. $repository->rid),
      l(t('Delete'), 'admin/settings/beanstalk/repositories/delete/'. $repository->rid),
    );
  }

  return theme('table', $headers, $rows);
}

/**
 * Load beanstalk repository object
 */
function beanstalk_repository_load($rid = NULL) {
  if (!empty($rid)) {
    $sql = "SELECT * FROM {beanstalk_repository} WHERE rid = %d";
    $result = db_fetch_object(db_query($sql, (int)$rid));
    if ($result) {
      $result->tids = unserialize($result->tids);
      $result->gids = unserialize($result->gids);
      $result->raw_data = unserialize($result->raw_data);
    }
  }

  return $result;
}

/**
 * Load beanstalk repository object by it beanstalk id.
 */
function beanstalk_repository_load_by_beanstalk_id($id = NULL) {
  $result = NULL;

  if (!empty($id)) {
    $sql = "SELECT * FROM {beanstalk_repository} WHERE beanstalk_id = %d";
    $result = db_fetch_object(db_query($sql, (int)$id));
    if ($result) {
      $result->tids = unserialize($result->tids);
      $result->gids = unserialize($result->gids);
      $result->raw_data = unserialize($result->raw_data);
    }
  }

  return $result;
}

/**
 * Save repository array
 */
function beanstalk_repository_save($repository = NULL) {
  $repository['tids'] = serialize($repository['tids']);
  $repository['gids'] = serialize($repository['gids']);
  
  if (!empty($repository['rid'])) {
    $result = drupal_write_record('beanstalk_repository', $repository, 'rid');
    watchdog('beanstalk', 'The repository %title has been updated.', array('%title' => $repository['title']), WATCHDOG_NOTICE);
  }
  else {
    $result = drupal_write_record('beanstalk_repository', $repository);
    watchdog('beanstalk', 'The repository %title has been created.', array('%title' => $repository['title']), WATCHDOG_NOTICE);
  }
  
  return $result;
}


/**
 * Delete Beanstalk repository
 */
function beanstalk_repository_delete($rid = NULL) {
  if (!empty($rid)) {
    $repository = beanstalk_repository_load($rid);
    $result = db_query('DELETE FROM {beanstalk_repository} WHERE rid = %d', $rid);
  }
  if ($result) {
    drupal_set_message(t('The repository %title has been deleted.', array('%title' =>  $repository->title)));
    watchdog('beanstalk', 'The repository %title has been deleted.', array('%title' =>  $repository->title), WATCHDOG_NOTICE);
  }
  else {
    drupal_set_message(t('There was an error deleting the repository.'), 'error');
  }
  drupal_goto('admin/settings/beanstalk/repositories');
}

/**
 * Beanstalk Webhook listener.
 */
function beanstalk_webhook() {
  /**
   * See if there is a token in $_GET, and if there is a matching repository.
   */
  if (isset($_GET['t']) and !empty($_GET['t'])) {
    $sql = "SELECT * FROM {beanstalk_repository} WHERE token = '%s'";
    $repository = db_fetch_object(db_query($sql, $_GET['t']));
    if (empty($repository) || !$repository->rid) {
      _beanstalk_die(t('Invalid token in $_GET.'));
    }
    else {
      $repository->tids = unserialize($repository->tids);
      $repository->gids = unserialize($repository->gids);
    }
  }
  else {
    _beanstalk_die(t('No token in $_GET.'));
  }

  /**
   * See if there is a JSON-commit in $_POST, get commit data.
   */
  if (isset($_REQUEST['payload']) and !empty($_REQUEST['payload'])) {
    $data = json_decode($_REQUEST['payload'], true);
    if (empty($data) or !is_array($data)) {
      _beanstalk_die(t('No valid JSON in $_REQUEST.'));
    }
  }
  else {
    _beanstalk_die(t('No payload in $_REQUEST.'));
  }

  /**
   * Check if is the first request Beanstalk do after register the webhook url.
   */
   if (isset($data['after']) && $data['after'] == '1111111111111111111111111111111111111111') {
     return;
   }

  /**
   * Everything is there, create the node!
   */
  if (isset($repository, $data)) {
    // Is a git push.
    if (isset($data['commits'])) {
      _beanstalk_commit_git($repository, $data);
    }
    // Is a svn commit.
    else {
      _beanstalk_commit($repository, $data);
    }
  }
}

/**
 * Returns the current webhook url.
 * @todo: Test if this works in hook_cron.
 */
function _beanstalk_webhook_url($token = FALSE) {
  $query = array();
  if ($token) {
    $query['t'] = $token;
  }

  return url(variable_get('beanstalk_webhook_url', 'beanstalk/webhook'), array('absolute' => TRUE, 'query' => $query));
}

/**
 * Generate an unique token to be used by Beanstalk.
 * We use this function to avoid get different tokens by different session ids.
 * The token will change when changing one of the following:
 *   - Beanstalk credentials (account, username and/or password)
 *   - Repository name
 *   - Drupal private key
 *
 * @param $name
 *   The beanstalk repository name.
 *
 * @return
 *   String with the generated token.
 *
 * @see drupal_get_token()
 */
function beanstalk_get_token($name) {
  list ($account, $username, $password) = beanstalk_autosync_credentials();
  $private_key = drupal_get_private_key();
  return md5(implode(':', array($account, $username, $password, $name, $private_key)));
}

/**
 * Sync repositories from a Beanstalk account.
 */
function beanstalk_repositories_sync() {
  module_load_include('inc', 'beanstalk', 'beanstalk.api_client');
  if (function_exists('drupal_queue_get')) {
    if (variable_get('beanstalk_autosync_processing', FALSE)) {
      return;
    }

    variable_set('beanstalk_autosync_processing', TRUE);
    $queue = drupal_queue_get('beanstalk_repositories_sync');
    $queue->createQueue();

    $repositories = beanstalk_resource_get('repositories');
    if ($repositories) {
      foreach ($repositories as $repository) {
        $data = array();
        $data['phase'] = 'repository';
        $data['repository'] = $repository;
        $queue->createItem($data);
      }
    }
    $data = array();
    $data['phase'] = 'finish';
    $queue->createItem($data);
  }
  else {
    $repositories = beanstalk_resource_get('repositories');
    if ($repositories) {
      foreach ($repositories as $repository) {
        beanstalk_repositories_sync_repository($repository);
      }
    }
  }
}

function beanstalk_repositories_sync_repository($repository) {
  module_load_include('inc', 'beanstalk', 'beanstalk.api_client');

  $repository = $repository->repository;
  $token = beanstalk_get_token($repository->name);
  $webhook_url = _beanstalk_webhook_url($token);

  $repository_data = array(
    'title' => $repository->name,
    'beanstalk_id' => $repository->id,
    'token' => $token,
    'raw_data' => serialize($repository),
  );
  if ($loaded_repository = beanstalk_repository_load_by_beanstalk_id($repository->id)) {
    $repository_data['rid'] = $loaded_repository->rid;
    drupal_write_record('beanstalk_repository', $repository_data, 'rid');
    watchdog('beanstalk', t('Repository @name updated by auto sync.', array('@name' => $repository->name)));
  }
  else {
    $repository_data['tids'] = serialize(array());
    $repository_data['gids'] = serialize(array());
    drupal_write_record('beanstalk_repository', $repository_data);
    watchdog('beanstalk', t('Repository @name automatically added by auto sync.', array('@name' => $repository->name)));
  }

  $integrations = beanstalk_resource_get("repositories/{$repository->name}/integrations");
  $webhook =
  $uptodate = FALSE;

  foreach ($integrations as $integration) {
    $integration = $integration->integration;

    if ($integration->type == 'WebHooksIntegration' && $integration->service_url == $webhook_url) {
      $webhook = TRUE;
      $uptodate = TRUE;
    }
    else if ($integration->type == 'WebHooksIntegration') {
      $webhook = $integration;
      $uptodate = FALSE;
    }
  }

  if ($webhook && $uptodate) {
    return;
  }
  else if ($webhook && !$uptodate) {
    beanstalk_resource_delete("repositories/{$repository->name}/integrations/{$webhook->id}");
    watchdog('beanstalk', t('Repository @name already contains a web hook pointing to @oldhook. Overriding to new @newhook.', array('@name' => $repository->name, '@oldhook' => $webhook->service_url, '@newhook' => $webhook_url)));
  }
  $post = beanstalk_resource_post("repositories/{$repository->id}/integrations", array(
    'integration' => array(
      'type' => 'WebHooksIntegration',
      'service_url' => $webhook_url,
    ),
  ));

  if (!isset($post->errors)) {
    watchdog('beanstalk', t('Added new web hook to repository @name pointing to @newhook.', array('@name' => $repository->name, '@newhook' => $webhook_url)));
  }
}

/**
 * Get saved credentials data.
 */
function beanstalk_autosync_credentials() {
  return array(
    variable_get('beanstalk_account', ''),
    variable_get('beanstalk_username', ''),
    variable_get('beanstalk_password', ''),
  );
}

/**
 * Get a given commit by its hash.
 *
 * @param $hash
 *   The complete commit hash.
 *
 * @return
 *   The commit node object.
 */
function beanstalk_commit_load_by_hash($hash) {
  $node = db_fetch_object(db_query("SELECT nid, vid FROM {beanstalk_node_commit} WHERE hash = '%s'", $hash));

  return node_load($node->nid, $node->vid);
}

/**
 * Get the title for a given commit message.
 *
 * @param $message
 *   The entire commit message.
 *
 * @return
 *   The resulted title.
 */
function beanstalk_commit_format_title($message) {
  $message = check_plain($message);
  $message = explode("\n", $message);
  return $message[0];
}

/**
 * Get Taxonomy vocabularies
 */
function _beanstalk_get_vocabularies() {
  $vocabularies = array(0 => t('None'));
  if (module_exists('taxonomy')) {
    $objects = taxonomy_get_vocabularies();
    foreach ($objects as $vocabulary) {
      if (in_array('beanstalk_commit', $vocabulary->nodes)) {
        $vocabularies[$vocabulary->vid] = $vocabulary->name;
      }
    }
  }
  return $vocabularies;
}

/**
 * Get Taxonomy terms
 */
function _beanstalk_get_terms() {
  $terms = array(0 => t('None'));
  $vid = variable_get('beanstalk_vid', 0);
  if (module_exists('taxonomy') && $vid != 0) {
    $sql = "SELECT t.* FROM {term_data} t WHERE t.vid = %d";
    $results = db_query(db_rewrite_sql($sql), $vid);
    while ($result = db_fetch_object($results)) {
      $terms[$result->tid] = $result->name;
    }
  }
  return $terms;
}

/**
 * Get OG groups
 */
function _beanstalk_get_groups() {
  $groups = array(0 => t('None'));
  if (module_exists('og') && og_is_group_post_type('beanstalk_commit')) {
    $og_groups = og_get_types('group');
    if (count($og_groups) > 0) {
      $placeholders = implode(',', array_fill(0, count($og_groups), "'%s'"));
      $sql = "SELECT n.nid FROM {node} n WHERE n.type IN ($placeholders)";
      $results = db_query(db_rewrite_sql($sql), $og_groups);
      while ($result = db_fetch_object($results)) {
        $node = node_load($result->nid);
        $groups[$node->nid] = $node->title;
      }
    }
  }
  return $groups;
}

/**
 * Wrapper for git push.
 *
 * Prepare git commit data and call _beanstalk_commit() for each one.
 */
function _beanstalk_commit_git($repository, $data) {
  foreach ($data['commits'] as $commit) {
    $commit['push'] = $data;
    $commit['revision'] = 0;
    $commit['time'] = $commit['timestamp'];

    $commit['author_full_name'] = $commit['author']['name'];
    $commit['author_email'] = $commit['author']['email'];
    $commit['author'] = t('@name <@mail>', array('@name' => $commit['author']['name'], '@mail' => $commit['author']['email']));

    _beanstalk_commit($repository, $commit);
  }
}

/**
 * Insert Beanstalk message as a node
 */
function _beanstalk_commit($repository, $data) {

  // Determine wether to link commits to Drupal users
  if (variable_get('beanstalk_author', 1) == 1) {
    $uid = _beanstalk_match_author($data['author'], $data['author_email']);
  }
  else {
    $uid = 0;
  }
  
  // Construct the node object
  $node                   = new stdClass();
  $node->type             = 'beanstalk_commit';
  $node->status           = variable_get('beanstalk_publish', 1);
  if (isset($data['id'])) {
    $node->title          = beanstalk_commit_format_title($data['message']);
    $node->hash           = $data['id'];
  }
  else {
    $node->title          = t('Revision @number', array('@number' => $data['revision']));
    $node->hash           = '';
  }
  $node->body             = '<p>'. $data['message'] .'</p>';
  $node->teaser           = node_teaser($node->body);
  $node->revision         = (int)$data['revision'];
  $node->time             = strtotime($data['time']);
  $node->changed_files    = serialize($data['changed_files']);
  $node->changed_dirs     = serialize($data['changed_dirs']);
  $node->changeset_url    = $data['changeset_url'];
  $node->author           = $data['author'];
  $node->author_full_name = $data['author_full_name'];
  $node->author_email     = $data['author_email'];
  $node->uid              = $uid;
  $node->raw_data         = serialize($data);
  
  if (module_exists('taxonomy') && variable_get('beanstalk_taxonomy', 0) == 1 && is_array($repository->tids)) {
    $node->taxonomy = array();
    foreach ($repository->tids as $tid) {
      $term = taxonomy_get_term($tid);
      if (is_object($term)) {
        $node->taxonomy[$tid] = taxonomy_get_term($tid);
      }
    }
  }
  
  if (module_exists('og') && variable_get('beanstalk_og', 0) == 1 && is_array($repository->gids) && og_is_group_post_type('beanstalk_commit')) {
    foreach ($repository->gids as $gid) {
      $group = node_load($gid);
      if (og_is_group_type($group->type)) {
        $node->og_groups = array($gid => $gid);
        $node->og_groups_both = array($gid => $group->title);
        $node->og_public = FALSE;
      }
    }
  }
  
  // Save the node,
  node_save(&$node);
  
  // Find and save node relations
  if ($node->nid) {
    $nids = _beanstalk_find_related($data['message']);
    foreach ($nids as $nid) {
      db_query("INSERT INTO {beanstalk_reference} (commit_nid, node_nid) VALUES (%d, %d)", $node->nid, $nid);
    }
  }
}

/**
 * Try and find commit author.
 */
function _beanstalk_match_author($name, $mail) {
  $user = user_load(array('name' => $name));
  if (!$user) {
    $user = user_load(array('mail' => $mail));
  }
  return $user ? $user->uid : 0;
}

/**
 * Find related nodes.
 */
function _beanstalk_find_related($message) {
  $matches = array();
  preg_match_all('/#\d+/', $message, &$matches);
  $nids = array();
  if (!empty($matches) and !empty($matches[0])) {
    foreach ($matches[0] as $match) {
      $nid = (int)substr($match, 1);
      if (node_load($nid)) {
        $nids[] = $nid;
      }
    }
  }
  return array_unique($nids);
}

/**
 * Helper function to replace tags with node links.
 */
function _beanstalk_link_nodes($message = '') {
  $nids = _beanstalk_find_related($message);
  $links = array();
  foreach ($nids as $nid) {
    $link = l('#'. $nid, 'node/'. $nid);
    $message = preg_replace('/#'. $nid .'/', $link, $message);
  }
  return $message;
}

/**
 * Log error, deny access
 */
function _beanstalk_die($message = '') {
  watchdog('beanstalk', 'Possible break-in attempt: %message', array('%message' => $message), WATCHDOG_ERROR);
  drupal_access_denied();
}
