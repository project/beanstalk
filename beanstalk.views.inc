<?php

/**
 * @file
 * Beanstalk -- views table descriptions
 */

function beanstalk_views_data() {
  
  // Table definition
  $data['beanstalk_node_commit']['table']['group'] = t('Beanstalk');
  
  // Table Relationships
  $data['beanstalk_node_commit']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // Node id field
  $data['beanstalk_node_commit']['nid'] = array(
    'title' => t('Beanstalk commit'),
    'help' => t('Beanstalk commit content that references a node.'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Beanstalk commit'),
    ),
  );
  
  // Date field.
  $data['beanstalk_node_commit']['time'] = array(
    'title' => t('Time'),
    'help' => t('Commit time.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  // Text field.
  $data['beanstalk_node_commit']['author'] = array(
    'title' => t('Author'),
    'help' => t('Commit author.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  
  // Text field.
  $data['beanstalk_node_commit']['author_full_name'] = array(
    'title' => t('Author name'),
    'help' => t('Commit author (full name).'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  
  // Text field.
  $data['beanstalk_node_commit']['author_full_email'] = array(
    'title' => t('Author email'),
    'help' => t('Commit author (full name).'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  
  // Text field.
  $data['beanstalk_node_commit']['changeset_url'] = array(
    'title' => t('Changeset URL'),
    'help' => t('Changeset overview URL.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  ); 

  return $data;
}

function beanstalk_views_plugins() {
}

function beanstalk_views_handlers() {
}